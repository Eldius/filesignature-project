package net.eldiosantos.filesignature.app.service;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

/**
 * Created by Eldius on 07/05/2017.
 */
public class HashAlgorithmListTest {
    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void list() throws Exception {
        List<String> algorithms = new HashAlgorithmList().list();

        algorithms.forEach(System.out::println);
        assertTrue("The result list contains the MD5 option?", algorithms.contains("MD5"));
        assertTrue("The result list contains the SHA option?", algorithms.contains("SHA"));
        assertTrue("The result list contains the SHA-512 option?", algorithms.contains("SHA-512"));
        assertTrue("The result list contains the SHA-256 option?", algorithms.contains("SHA-256"));
    }
}
