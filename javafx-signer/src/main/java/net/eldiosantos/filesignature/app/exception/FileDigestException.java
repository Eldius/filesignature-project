package net.eldiosantos.filesignature.app.exception;

/**
 * Created by Eldius on 07/05/2017.
 */
public class FileDigestException extends Exception {
    public FileDigestException() {
    }

    public FileDigestException(String message) {
        super(message);
    }

    public FileDigestException(String message, Throwable cause) {
        super(message, cause);
    }

    public FileDigestException(Throwable cause) {
        super(cause);
    }

    public FileDigestException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
