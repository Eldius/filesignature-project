package net.eldiosantos.filesignature.app.service;

import java.security.Provider;
import java.security.Security;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Eldius on 07/05/2017.
 */
public class HashAlgorithmList {
    public List<String> list() {
        return Arrays.stream(Security.getProviders())
                .flatMap(p -> p.getServices().stream())
                .filter(s -> "MessageDigest".equals(s.getType()))
                .map(Provider.Service::getAlgorithm)
                .collect(Collectors.toList());
    }
}
