package net.eldiosantos.filesignature.app.controller;

import com.sun.javafx.collections.ObservableListWrapper;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import net.eldiosantos.filesignature.app.exception.FileDigestException;
import net.eldiosantos.filesignature.app.service.FileDigester;
import net.eldiosantos.filesignature.app.service.HashAlgorithmList;
import net.eldiosantos.filesignature.app.service.SaveSignatureFile;
import net.eldiosantos.filesignature.app.service.VerifyFileSignature;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Eldius on 07/05/2017.
 */
public class MainController implements Initializable {
    @FXML
    private AnchorPane mainPane;
    @FXML
    private Button btnChooseFile;
    @FXML
    private Button btnSignature;
    @FXML
    private ComboBox<String> cmbAlgorithm;
    @FXML
    private TextField txtFileName;
    @FXML
    private TextField txtHashSignature;
    @FXML
    private TextArea txtLogPanel;
    @FXML
    private Button btnVerify;

    private File sourceFile;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        cmbAlgorithm.setItems(new ObservableListWrapper<String>(new HashAlgorithmList().list()));
    }

    public void chooseFile(final ActionEvent actionEvent) {
        final FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        sourceFile = fileChooser.showOpenDialog(mainPane.getScene().getWindow());

        txtFileName.setText(sourceFile.getName());
    }

    public void generateSinature(final ActionEvent actionEvent) throws FileDigestException {
        final String algorithm = cmbAlgorithm.getSelectionModel().getSelectedItem();
        final String digest = new FileDigester().digest(sourceFile.toPath(), algorithm);
        txtLogPanel.setText(String.format("%s\n%s [%s] => %s\n", txtLogPanel.getText(), sourceFile.getName(), algorithm, digest));
        txtHashSignature.setText(digest);

        try {
            new SaveSignatureFile().save(sourceFile, digest, algorithm);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void verifyFileSignature(final ActionEvent actionEvent) throws FileDigestException, IOException {
        final String algorithm = cmbAlgorithm.getSelectionModel().getSelectedItem();
        final Boolean verify = new VerifyFileSignature().verify(sourceFile, cmbAlgorithm.getSelectionModel().getSelectedItem());
        txtLogPanel.setText(String.format("%s\n%s [%s] => %s\n", txtLogPanel.getText(), sourceFile.getName(), algorithm, verify));
    }
}
