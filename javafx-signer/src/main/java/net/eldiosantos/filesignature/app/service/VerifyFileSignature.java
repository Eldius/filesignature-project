package net.eldiosantos.filesignature.app.service;

import net.eldiosantos.filesignature.app.exception.FileDigestException;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;

/**
 * Created by Eldius on 07/05/2017.
 */
public class VerifyFileSignature {
    public Boolean verify(final File source, final String algorithm) throws FileDigestException, IOException {

        final String digest = new FileDigester().digest(source.toPath(), algorithm);

        final String savedDigest = Files.readAllLines(new SignatureFilePath().getPath(algorithm, source.getAbsolutePath()), Charset.forName("utf-8")).get(0);

        return digest.equals(savedDigest);
    }
}
