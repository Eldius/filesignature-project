package net.eldiosantos.filesignature.app.service;

import net.eldiosantos.filesignature.app.exception.FileDigestException;

import java.io.InputStream;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.DigestInputStream;
import java.security.MessageDigest;

/**
 * Created by Eldius on 07/05/2017.
 */
public class FileDigester {

    public String digest(final Path file, final String algorithm) throws FileDigestException {
        try {
            MessageDigest md = MessageDigest.getInstance(algorithm);
            try (
                InputStream is = Files.newInputStream(file);
                DigestInputStream dis = new DigestInputStream(is, md)
            ) {
                // ??
            }
            BigInteger bigInt = new BigInteger(1,md.digest());
            return bigInt.toString(16).toUpperCase();
        } catch (Exception e) {
            throw new FileDigestException("Error generating file's hash", e);
        }
    }
}
