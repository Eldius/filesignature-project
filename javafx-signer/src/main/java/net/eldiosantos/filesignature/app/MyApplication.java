package net.eldiosantos.filesignature.app;/**
 * Created by Eldius on 07/05/2017.
 */

import com.sun.javafx.collections.ObservableListWrapper;
import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import net.eldiosantos.filesignature.app.service.HashAlgorithmList;

import java.nio.file.Paths;

public class MyApplication extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    private Stage primaryStage;
    private AnchorPane rootLayout;

    @Override
    public void start(Stage primaryStage) {

        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("AddressApp");

        initRootLayout();

    }

    private void initRootLayout() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(ClassLoader.getSystemResource("view/main.fxml"));

            rootLayout = loader.load();

            final Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);
            primaryStage.show();

            initFormData();

        } catch (Exception e) {
            e.printStackTrace();
            throw new IllegalStateException(e);
        }
    }

    private void initFormData() {

    }
}
