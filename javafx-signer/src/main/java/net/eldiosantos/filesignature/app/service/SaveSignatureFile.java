package net.eldiosantos.filesignature.app.service;

import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by Eldius on 07/05/2017.
 */
public class SaveSignatureFile {
    public void save(final File source, final String signature, final String algorithm) throws IOException {
        final String absolutePath = source.getAbsolutePath();
        final Path path = new SignatureFilePath().getPath(algorithm, absolutePath);
        createFile(path);
        try (OutputStreamWriter writer = new OutputStreamWriter(Files.newOutputStream(path))) {
            writer.append(signature);
        }
    }

    private void createFile(Path path) throws IOException {
        if(Files.exists(path)) {
            Files.delete(path);
        }
        Files.createFile(path);
    }
}
