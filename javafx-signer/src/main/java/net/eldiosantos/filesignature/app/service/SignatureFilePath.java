package net.eldiosantos.filesignature.app.service;

import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by Eldius on 07/05/2017.
 */
public class SignatureFilePath {
    public Path getPath(String algorithm, String absolutePath) {
        return Paths.get(absolutePath.substring(0, absolutePath.lastIndexOf('.') + 1) + algorithm.toLowerCase());
    }


}
